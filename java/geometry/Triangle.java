package geometry;

import java.util.ArrayList;     // [[aggiunta libreria per ArrayList]]
import java.lang.Math;          // [[aggiunta libreria per classe Math]]

class Triangle extends Polygon{

    public Triangle(ArrayList<Double> sides){           // [[costruttore di prima]] 
        super(sides);                                   // [[perche' non c'e' base e altezza?]]
    } 


    //implement abstract method of super class
    public double computeArea(){
        double p = this.computePerimeter()/2;           // [[calcolo semiperimetro]]
        double a = sides.get(0);                        // [["prendo" i valori dei lati dall'arrayList, 0,1,2]]
        double b = sides.get(1);
        double c = sides.get(2);
        return Math.sqrt(p*(p-a)*(p-b)*(p-c));         // [[formula di Erone per calcolo area]]
    }

}