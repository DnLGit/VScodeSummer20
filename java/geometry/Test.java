package geometry;

import java.util.ArrayList;

class Test{

    public static void main(String[] args){

        System.out.println("\n***** ArrayList e Poligoni *****\n");

        ArrayList<Double> sidesTri = new ArrayList<>();
        sidesTri.add(50.0);
        sidesTri.add(35.0);
        sidesTri.add(25.0);
       

        Triangle tr = new Triangle(sidesTri);
        System.out.println("Perimetro Triangle: " + tr.computePerimeter());
        System.out.println("Area Triangle: "+ tr.computeArea());

        ArrayList<Double> sidesTriso = new ArrayList<>();
        sidesTriso.add(50.0);
        sidesTriso.add(35.0);
        sidesTriso.add(35.0);
        
        Triangle triso = new TriangleIso(sidesTriso);
        System.out.println("Perimetro TriangleIso: " + triso.computePerimeter());
        System.out.println("Area TriangleIso: "+ triso.computeArea());
        
        ArrayList<Double> sidesTrieq = new ArrayList<>();
        sidesTrieq.add(50.0);
        sidesTrieq.add(50.0);
        sidesTrieq.add(50.0);

        Triangle treq = new TriangleEqui(sidesTrieq);
        System.out.println("Perimetro TriangleEq: " + treq.computePerimeter());
        System.out.println("Area TriangleEq: "+ treq.computeArea());
        
        return;
    }
}