package geometry;

import java.util.ArrayList;     // [[aggiunta libreria per ArrayList]]
import java.lang.Math;          // [[aggiunta libreria per funzioni di Math]]

class TriangleRect extends Triangle{

    public TriangleRect(ArrayList<Double> sides){       // [[costruttore di prima]] 
        super(sides);                                   // [[perche' non c'e' base e altezza?]]
    } 

    

    public double getCatetoMinore(){                     // calcolo valore minore con funzione Math.min
        return Math.min( Math.min(sides.get(0),sides.get(1)), sides.get(2));
    }


    public double getCatetoMaggiore(){  // calcolo valore medio con funzione "Math.max"
        double ip = getIpotenusa();     // [trovo il max tra i due lati esclusa l'ipotenusa a indice 0 ....]
        if(ip == sides.get(0))
            return Math.max(sides.get(1), sides.get(2));
        
        else if(ip == sides.get(1))                         // [oppure a indice 1 ...]
            return Math.max(sides.get(0), sides.get(2));
        
        else                                                // [oppure se l'ipotenusa e' in posizione 2 ...!!!]
            return Math.max(sides.get(0), sides.get(1));
    }


    public double getIpotenusa(){                                           // calcolo valore maggiore con funzione Math.max
        return Math.max( Math.max(sides.get(0),sides.get(1)), sides.get(2));    // il maggiore del maggiore fra i due ...
    }


    //implement abstract method of super class
    public double computeArea(){
        return this.getCatetoMinore()*this.getCatetoMaggiore()/2;
    }
}