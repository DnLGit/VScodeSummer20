package geometry;

import java.util.ArrayList;

abstract class Polygon {
    protected ArrayList<Double> sides; // each item will contain the length value for a side of the polygon

    public Polygon(ArrayList<Double> sides){    //here I know the measures for each side
        this.sides = sides;
    }

    public double computePerimeter(){   // [[metodo "non" astratto perché valido per tutte le figure ]]
        double sum = 0.0;
        for(int i=0; i < sides.size(); i++)
            sum += sides.get(i);        // [[qui "aggiungo" il valore dei lati "ottenuti" dall'arrayList]]

        return sum;
    }

    public abstract double computeArea();
    
}