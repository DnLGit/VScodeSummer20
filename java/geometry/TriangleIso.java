package geometry;

import java.util.ArrayList;     // [[aggiunta libreria per ArrayList]]
import java.lang.Math;          // [[aggiunta libreria per funzioni di Math]]

class TriangleIso extends Triangle {


    public TriangleIso(ArrayList<Double> sides){
        super(sides);
    }

    public double getBase(){                // trovo la base che non e' uguale ai due lati.
        if(sides.get(1) == sides.get(2))
            return sides.get(0);
        
        else if(sides.get(0) == sides.get(2))
            return sides.get(1);
        
        else return sides.get(2);
    }

    public double getLato(){                // trovo uno dei due lati ... diversi dalla base.
        double base = getBase();
        if(sides.get(0) != base)
            return sides.get(0);

        else if(sides.get(1) != base)
            return sides.get(1);

        else return sides.get(2);
    }

    public double getHeight(){              // calcolo dell'altezza con Pitagora.
        double base = getBase();
        double lato = getLato();
        return Math.sqrt(lato*lato - (base/2)*(base/2));
    }

    public double getHeight2(){             // come sopra ma anche con la funzione .pow di Math.
        return Math.sqrt(Math.pow(getLato(), 2) - Math.pow(getBase()/2, 2));
    }

    public double computeArea(){            // overRide del metodo "computeArea".
        return this.getBase()*this.getHeight()/2;
    }
    





/*     public double getHeight2(){             // come sopra ma anche con la funzione .pow di Math.
        double base = getBase();
        double lato = getLato();
        return Math.sqrt(Math.pow(lato, 2) - Math.pow(base/2, 2)); */
}
